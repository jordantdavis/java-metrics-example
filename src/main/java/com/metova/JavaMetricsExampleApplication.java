package com.metova;

import com.metova.resources.DeveloperResource;
import com.metova.resources.DeveloperResourceAnnotated;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class JavaMetricsExampleApplication extends Application<JavaMetricsExampleConfiguration> {

    public static void main(final String[] args) throws Exception {
        new JavaMetricsExampleApplication().run(args);
    }

    @Override
    public String getName() {
        return "java-metrics-example";
    }

    @Override
    public void initialize(final Bootstrap<JavaMetricsExampleConfiguration> bootstrap) {
        // TODO: application initialization
    }

    @Override
    public void run(final JavaMetricsExampleConfiguration configuration,
                    final Environment environment) {
        final DeveloperResource developerResource = new DeveloperResource(environment.metrics());

        environment.jersey().register(developerResource);
    }
}
