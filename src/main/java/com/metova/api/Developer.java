package com.metova.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Developer {
    private long id;
    private String name;
    private String specialty;

    public Developer(long id, String name, String specialty) {
        this.id = id;
        this.name = name;
        this.specialty = specialty;
    }

    @JsonProperty
    public long getId() {
        return id;
    }

    @JsonProperty
    public String getName() {
        return name;
    }

    @JsonProperty
    public String getSpecialty() {
        return specialty;
    }
}
