package com.metova.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class DevelopersList {
    private List<Developer> developers;

    public DevelopersList(List<Developer> developers) {
        this.developers = developers;
    }

    @JsonProperty
    public List<Developer> getDevelopers() {
        return developers;
    }
}
