package com.metova.resources;

import com.codahale.metrics.Counter;
import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.metova.api.Developer;
import com.metova.api.DevelopersList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Path("/developers")
@Produces(MediaType.APPLICATION_JSON)
public class DeveloperResource {
    private static final List<Developer> DEVELOPERS = createDevelopersList();

    private static final String GET_ALL_DEVELOPERS_TIMER_METRIC = name("getAllDevelopers");
    private static final String GET_DEVELOPER_COUNTER_METRIC = name("getDeveloper.count");
    private static final String GET_DEVELOPER_SUCCESS_METER_METRIC = name("getDeveloper.success");
    private static final String GET_DEVELOPER_FAILURE_METER_METRIC = name("getDeveloper.failure");

    private final Timer getAllDevsTimer;
    private final Counter getDevCounter;
    private final Meter getDevSuccessMeter;
    private final Meter getDevFailureMeter;

    public DeveloperResource(MetricRegistry metricRegistry) {
        this.getAllDevsTimer = metricRegistry.timer(GET_ALL_DEVELOPERS_TIMER_METRIC);
        this.getDevCounter = metricRegistry.counter(GET_DEVELOPER_COUNTER_METRIC);
        this.getDevSuccessMeter = metricRegistry.meter(GET_DEVELOPER_SUCCESS_METER_METRIC);
        this.getDevFailureMeter = metricRegistry.meter(GET_DEVELOPER_FAILURE_METER_METRIC);
    }

    @GET
    public DevelopersList getAllDevelopers() {
        // start timer
        Timer.Context timerContext = getAllDevsTimer.time();

        DevelopersList developersList = new DevelopersList(DEVELOPERS);

        // stop timer
        timerContext.stop();

        return developersList;
    }

    @GET
    @Path("/{id}")
    public Developer getDeveloper(@PathParam("id") long id) {
        getDevCounter.inc();

        Optional<Developer> possibleDeveloper = DEVELOPERS
                .stream()
                .filter(developer -> developer.getId() == id)
                .findFirst();

        if (possibleDeveloper.isPresent()) {
            getDevSuccessMeter.mark();

            return possibleDeveloper.get();
        }

        getDevFailureMeter.mark();

        throw new NotFoundException("Developer not found.");
    }

    private static List<Developer> createDevelopersList() {
        List<Developer> developers = new ArrayList<>();

        developers.add(new Developer(1, "Jordan Davis", "Java"));
        developers.add(new Developer(2, "Mark Mynsted", "Scala"));
        developers.add(new Developer(3, "David Knapp", "Java"));
        developers.add(new Developer(4, "Matthew Burton", "C#"));
        developers.add(new Developer(5, "Wesley Choate", "Java"));

        return developers;
    }

    private static final String name(String identifier) {
        return DeveloperResource.class.getCanonicalName() + "." + identifier;
    }
}
